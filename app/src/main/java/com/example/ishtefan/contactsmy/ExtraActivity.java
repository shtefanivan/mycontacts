package com.example.ishtefan.contactsmy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.Serializable;



/**
 * Created by ishtefan on 16.10.2016.
 */


public class ExtraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extra_contact_info);

         Contact contact = (Contact) getIntent().getSerializableExtra("item");//получаем добавленное значение, указав ключ
/*
        String tel = getIntent().getExtras().getString("Tel");
        String adr = getIntent().getExtras().getString("Address");
        String email = getIntent().getExtras().getString("Email");
*/
        TextView ec_name = (TextView) findViewById(R.id.ok_name);
        TextView ec_tel = (TextView) findViewById(R.id.tel);
        TextView ec_adr = (TextView) findViewById(R.id.address);
        TextView ec_email = (TextView) findViewById(R.id.ok_email);

        ec_name.setText(contact.getName());
        ec_email.setText(contact.getEmail());
        ec_adr.setText(contact.getAddress());
        ec_tel.setText(contact.getTel());

    }
}
