package com.example.ishtefan.contactsmy;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by ishtefan on 15.10.2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<Contact> list;
    private OnRecyclerItemClickListener listener;
    private Context context;


    public interface OnRecyclerItemClickListener {
        void onItemClickListener(Contact item, int position);
    }

    public RecyclerViewAdapter(ArrayList<Contact> spisok, Context context) {
        this.list = spisok;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.one_contact_view, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact contact = list.get(position);
        holder.name.setText(contact.getName());
        holder.email.setText(contact.getEmail());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListener(OnRecyclerItemClickListener listener) {
        this.listener = listener;
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private TextView email;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.ok_name);
            email = (TextView) itemView.findViewById(R.id.ok_email);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (listener != null) {
                int position = getAdapterPosition();
                listener.onItemClickListener(list.get(position), position);
            } else {
                throw new RuntimeException("You must init OnRecyclerItemClickListener by calling setListener() method.");
            }
        }
    }
}
