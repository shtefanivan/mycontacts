package com.example.ishtefan.contactsmy;
/*
1. Создать список контактов. Каждый контакт содержит имя, email,
        адрес и телефон иконку (или фотку). Список содержит только иконку, имя и email контакта.
        При нажатии на контакт - он должен открываться в новом activity с подробной
        о нем информацией. Использовать ListView.
*/


import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;


import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Contact> contacts = generateContacts();

        recyclerView = (RecyclerView) findViewById(R.id.recycle_list_main);
        LinearLayoutManager linearLayoutManagerManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManagerManager);
        adapter = new RecyclerViewAdapter(contacts, this);

        final Intent intent = new Intent(this, ExtraActivity.class);

        adapter.setListener(new RecyclerViewAdapter.OnRecyclerItemClickListener() {
            @Override
            public void onItemClickListener(Contact item, int position) {
                Toast.makeText(MainActivity.this, item.getName(), Toast.LENGTH_SHORT).show();
                intent.putExtra("item", item);
/*
                intent.putExtra("Tel", item.getTel());
                intent.putExtra("Address", item.getAddress());
                intent.putExtra("Email", item.getEmail());
*/
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @NonNull
    private ArrayList<Contact> generateContacts() {

        ArrayList<Contact> contacts = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Contact contact = new Contact(i + " name", i + "cont@mail.com", "+380 000 222 00" + i, 1 + "Иконка", "address " + i);
            contacts.add(contact);
        }
        return contacts;
    }

}

