package com.example.ishtefan.contactsmy;

import android.graphics.drawable.Icon;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by ishtefan on 07.10.2016.
 */

public class Contact implements Serializable{
    private String name;
    private String email;
    private String address;
    private String tel;
    private String icon;

    public Contact(String name, String email, String tel, String icon, String address) {
        this.name = name;
        this.email = email;
        this.tel = tel;
        this.icon = icon;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getTel() {
        return tel;
    }

    public String getIcon() {
        return icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
